import {Thread} from './thread';
import {Reply} from './reply';

export interface Activity {

  id: number;
  user_id: number;
  subject_id: number;
  subject?: Thread | Reply;
  subject_type: string;
  type: string;
  made_at?: string;
  created_at?: Date;
  updated_at?: Date;

}
