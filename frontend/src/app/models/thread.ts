import { User } from './user';
import {Reply} from './reply';
import {Channel} from './channel';

export interface Thread {

  id?: number;
  user_id?: number;
  creator?: User;
  channel_id: number;
  channel?: Channel;
  replies_data?: {
    current_page: number,
    last_page: number,
    items: Array<Reply>
  };
  replies_count?: number;
  subscribed?: boolean;
  title: string;
  body: string;
  can_update?: boolean;
  has_updates?: boolean;
  created_at?: Date;
  published_at?: string;
  updated_at?: Date;

}
