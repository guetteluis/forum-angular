import {Thread} from './thread';
import {Reply} from './reply';

export interface Notification {

  id: string;
  type: string;
  notifiable_id: number;
  notifiable_type: string;
  data: {
    message: string,
    channel_slug: string,
    thread_id: number
  };
  read_at: Date;
  created_at: Date;
  updated_at: Date;

}
