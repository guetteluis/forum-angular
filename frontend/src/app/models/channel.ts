import {Thread} from './thread';

export interface Channel {

  id?: number;
  name: string;
  slug: string;
  threads?: Array<Thread>;
  created_at?: Date;
  updated_at?: Date;

}
