import {Activity} from './activity';

export interface User {

  id?: number;
  name: string;
  email: string;
  password?: string;
  avatar_path?: string;
  activities_data?: {
    current_page: number,
    last_page: number,
    items: Array<Activity>
  };
  since_at?: string;
  created_at?: Date;
  updated_at?: Date;

}
