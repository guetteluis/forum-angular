import { User } from './user';
import {Thread} from './thread';

export interface Reply {

  id?: number;
  user_id?: number;
  owner?: User;
  thread_id?: number;
  thread?: Thread;
  favorites_count?: number;
  favorited?: boolean;
  mentioned_usernames?: Array<string>;
  body: string;
  can_update?: boolean;
  created_at?: Date;
  updated_at?: Date;
  posted_at?: string;

}
