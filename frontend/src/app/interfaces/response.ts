import {Thread} from '../models/thread';
import {User} from '../models/user';
import {Channel} from '../models/channel';
import {Reply} from '../models/reply';
import {Notification} from '../models/notification';

export interface Response {

  errors?: any;
  message?: string;
  channels?: Array<Channel>;
  threads?: Array<Thread>;
  thread?: Thread;
  users?: Array<string>;
  user?: User;
  profile?: User;
  notifications?: Array<Notification>;
  threads_data?: {
    current_page: number,
    last_page: number,
    items: Array<Thread>
  };
  replies_data?: {
    current_page: number,
    last_page: number,
    items: Array<Reply>
  };

}
