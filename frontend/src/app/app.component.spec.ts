import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import {NavbarComponent} from './components/navbar/navbar.component';
import {ProgressBarComponent} from './components/progress-bar/progress-bar.component';
import {RouterModule} from '@angular/router';
import {AppRouting} from './app.routes';
import {HomeComponent} from './components/home/home.component';
import {ThreadsIndexComponent} from './components/threads-index/threads-index.component';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatCardModule, MatProgressBarModule} from '@angular/material';
import {BrowserModule} from '@angular/platform-browser';
describe('AppComponent', () => {

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        NavbarComponent,
        HomeComponent,
        ThreadsIndexComponent,
        ProgressBarComponent
      ],
      imports: [
        BrowserModule,
        BrowserAnimationsModule,
        AppRouting,
        HttpClientModule,
        MatCardModule,
        MatProgressBarModule
      ],
    }).compileComponents();
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it(`should have as title 'app'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('app');
  }));

  it('should render title in a h1 tag', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Welcome to app!');
  }));
});
