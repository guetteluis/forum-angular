import {Injectable} from '@angular/core';
import {HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class ResponseInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    return next.handle(req).catch((response: any, caught) => {

      if (response instanceof HttpErrorResponse && response.error.errors) {
        const errors: Array<string> = [];

        for (const key in response.error.errors) {
          if (response.error.errors[key]) {
            for (const item of response.error.errors[key]) {
              errors.push(item);
            }
          }
        }
        return Observable.throw(errors);
      }
      return Observable.throw(response);

    });

  }
}
