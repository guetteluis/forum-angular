import {Directive, ElementRef} from '@angular/core';

declare let $: any;

@Directive({
  selector: '[data-toggle="popover"]'
})
export class TooltipDirective {

  constructor(el: ElementRef) {
    $(function () {
      $('[data-toggle="popover"]').popover();
    });
  }

}
