import {Directive, ElementRef, HostListener} from '@angular/core';
import {Router} from '@angular/router';

@Directive({
  selector: '[appRoute]'
})
export class RouteDirective {

  constructor(
    private el: ElementRef, private router: Router
  ) { }

  @HostListener('click', ['$event.target.parentElement']) onClick($event) {

    const goRoute = $event.getAttribute('data-link');
    this.router.navigate([goRoute]);

  }

}
