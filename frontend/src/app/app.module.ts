import { BrowserModule } from '@angular/platform-browser';
import { polyfill as keyboardEventKeyPolyfill } from 'keyboardevent-key-polyfill';
import { TextInputAutocompleteModule } from 'angular-text-input-autocomplete';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatButtonModule, MatCardModule, MatFormFieldModule, MatIconModule, MatInputModule, MatMenuModule, MatProgressBarModule,
  MatSelectModule
} from '@angular/material';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { AppRouting } from './app.routes';
import { ResponseInterceptor } from './interceptors/response/response-interceptor';

import { WrapUsernamesWithinLinksPipe } from './pipes/wrap-usernames-within-links/wrap-usernames-within-links.pipe';
import { SafeHtmlPipe } from './pipes/safe-html/safe-html.pipe';

import { TooltipDirective } from './directives/tooltip/tooltip.directive';
import { RouteDirective } from './directives/route/route.directive';

import { AuthGuard } from './guards/auth/auth.guard';

import { HeadersService } from './services/headers/headers.service';
import { ThreadsService } from './services/threads/threads.service';
import { ProgressBarService } from './services/progress-bar/progress-bar.service';
import { AuthService } from './services/auth/auth.service';
import { RepliesService } from './services/replies/replies.service';
import { ChannelsService } from './services/channels/channels.service';
import { FavoritesService } from './services/favorites/favorites.service';
import { ProfilesService } from './services/profiles/profiles.service';
import { FlashService } from './services/flash/flash.service';
import { ThreadSubscriptionsService } from './services/thread-subscriptions/thread-subscriptions.service';
import { NotificationsService } from './services/notifications/notifications.service';
import { UsersService } from './services/users/users.service';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { HomeComponent } from './components/home/home.component';
import { ThreadsIndexComponent } from './components/threads-index/threads-index.component';
import { ProgressBarComponent } from './components/progress-bar/progress-bar.component';
import { ThreadsShowComponent } from './components/threads-show/threads-show.component';
import { LoginComponent } from './components/login/login.component';
import { ThreadsCreateComponent } from './components/threads-create/threads-create.component';
import { FormErrorsAlertComponent } from './components/form-errors-alert/form-errors-alert.component';
import { PaginatorComponent } from './components/paginator/paginator.component';
import { ProfilesShowComponent } from './components/profiles-show/profiles-show.component';
import { ActivitiesComponent } from './components/activities/activities.component';
import { FlashComponent } from './components/flash/flash.component';
import { ReplyComponent } from './components/reply/reply.component';
import { FavoriteComponent } from './components/favorite/favorite.component';
import { NewReplyComponent } from './components/new-reply/new-reply.component';
import { RepliesComponent } from './components/replies/replies.component';
import { SubscribeButtonComponent } from './components/subscribe-button/subscribe-button.component';
import { UserNotificationsComponent } from './components/user-notifications/user-notifications.component';
import { ThreadComponent } from './components/thread/thread.component';

keyboardEventKeyPolyfill();

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    ThreadsIndexComponent,
    ProgressBarComponent,
    ThreadsShowComponent,
    LoginComponent,
    TooltipDirective,
    ThreadsCreateComponent,
    FormErrorsAlertComponent,
    PaginatorComponent,
    ProfilesShowComponent,
    ActivitiesComponent,
    FlashComponent,
    ReplyComponent,
    FavoriteComponent,
    NewReplyComponent,
    RepliesComponent,
    SubscribeButtonComponent,
    UserNotificationsComponent,
    WrapUsernamesWithinLinksPipe,
    SafeHtmlPipe,
    RouteDirective,
    ThreadComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRouting,
    HttpClientModule,
    MatCardModule,
    MatProgressBarModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatMenuModule,
    MatSelectModule,
    MatIconModule,
    TextInputAutocompleteModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ResponseInterceptor,
      multi: true,
    },
    HeadersService,
    ThreadsService,
    ProgressBarService,
    AuthService,
    RepliesService,
    AuthGuard,
    ChannelsService,
    FavoritesService,
    ProfilesService,
    FlashService,
    ThreadSubscriptionsService,
    NotificationsService,
    UsersService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
