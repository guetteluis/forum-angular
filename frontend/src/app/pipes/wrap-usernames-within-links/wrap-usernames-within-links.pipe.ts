import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'wrapUsernamesWithinLinks'
})
export class WrapUsernamesWithinLinksPipe implements PipeTransform {

  transform(value: any, usernames: Array<string>): any {

    for (const username of usernames) {
      value = value.replace(
        `@${username}`,
        `<a class="link-secondary" data-link="/profiles/${username}"><b>@${username}</b></a>`);
    }

    return value;

  }

}
