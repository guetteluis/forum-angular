import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { HeadersService } from '../headers/headers.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { Response } from '../../interfaces/response';
import {environment} from '../../../environments/environment';
import {Thread} from '../../models/thread';
import {AuthService} from '../auth/auth.service';

@Injectable()
export class ThreadsService {

  protected apiUrl: string = environment.api_url;

  constructor(
    private http: HttpClient,
    private headersService: HeadersService,
    private auth: AuthService
  ) { }

  index(queryParams: any, channelSlug?: string): Observable<Response> {

    if (!channelSlug) {
      channelSlug = '';
    }

    this.headersService.set();
    this.headersService.addAuthHeader(this.auth.getTokenType() + ' ' + this.auth.getToken());

    return this.http.get(this.apiUrl + '/api/threads/' + channelSlug,
      {
        headers: this.headersService.get(),
        params: queryParams
      })
      .map(response => response)
      .catch((error: any) => Observable.throw(error));

  }

  show(channel_slug: string, id: number, searchParams: any): Observable<Response> {

    this.headersService.set();
    this.headersService.addAuthHeader(this.auth.getTokenType() + ' ' + this.auth.getToken());

    return this.http.get(
      this.apiUrl + '/api/threads/' + channel_slug + '/' + id,
      {headers: this.headersService.get(), params: searchParams}
      )
      .map(response => response)
      .catch((error: any) => Observable.throw(error));

  }

  store(thread: Thread): Observable<Response> {

    this.headersService.set();
    this.headersService.addAuthHeader(this.auth.getTokenType() + ' ' + this.auth.getToken());

    return this.http.post(this.apiUrl + '/api/threads', thread, {headers: this.headersService.get()})
      .map(response => response)
      .catch((errors: any) => Observable.throw(errors));

  }

  destroy(channel_slug: string, thread_id): Observable<Response> {

    this.headersService.set();
    this.headersService.addAuthHeader(this.auth.getTokenType() + ' ' + this.auth.getToken());

    return this.http.delete(this.apiUrl + '/api/threads/' + channel_slug + '/' + thread_id, {headers: this.headersService.get()})
      .map(response => response)
      .catch((errors: any) => Observable.throw(errors));

  }

}
