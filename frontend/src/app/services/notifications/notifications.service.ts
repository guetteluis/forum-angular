import { Injectable } from '@angular/core';
import {AuthService} from '../auth/auth.service';
import {environment} from '../../../environments/environment';
import {HeadersService} from '../headers/headers.service';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Response} from '../../interfaces/response';

@Injectable()
export class NotificationsService {

  protected apiUrl: string = environment.api_url;

  constructor(
    private http: HttpClient,
    private headersService: HeadersService,
    private auth: AuthService
  ) { }

  index(profileName: string): Observable<Response> {

    this.headersService.set();
    this.headersService.addAuthHeader(this.auth.getTokenType() + ' ' + this.auth.getToken());

    return this.http.get(
      this.apiUrl + '/api/profiles/' + profileName + '/notifications',
      {headers: this.headersService.get()}
    ).map(response => response)
     .catch((error: any) => Observable.throw(error));

  }

  destroy(profileName: string, notificationId: string): Observable<Response> {

    this.headersService.set();
    this.headersService.addAuthHeader(this.auth.getTokenType() + ' ' + this.auth.getToken());

    return this.http.delete(
      this.apiUrl + '/api/profiles/' + profileName + '/notifications/' + notificationId,
      {headers: this.headersService.get()}
    ).map(response => response)
      .catch((error: any) => Observable.throw(error));

  }

}
