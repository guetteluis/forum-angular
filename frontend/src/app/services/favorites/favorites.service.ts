import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {HeadersService} from '../headers/headers.service';
import {environment} from '../../../environments/environment';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Reply} from '../../models/reply';
import {Observable} from 'rxjs/Observable';
import {AuthService} from '../auth/auth.service';
import {Response} from '../../interfaces/response';

@Injectable()
export class FavoritesService {

  protected apiUrl: string = environment.api_url;

  constructor(
    private http: HttpClient,
    private headersService: HeadersService,
    private auth: AuthService
  ) { }

  store(reply_id: number): Observable<Response> {

    this.headersService.set();
    this.headersService.addAuthHeader(this.auth.getTokenType() + ' ' + this.auth.getToken());

    return this.http.post(
      this.apiUrl + '/api/replies/' + reply_id + '/favorites', {},
      {headers: this.headersService.get()}
    )
      .map(response => response)
      .catch((error: any) => Observable.throw(error));

  }

  destroy(reply_id: number): Observable<Response> {

    this.headersService.set();
    this.headersService.addAuthHeader(this.auth.getTokenType() + ' ' + this.auth.getToken());

    return this.http.delete(
      this.apiUrl + '/api/replies/' + reply_id + '/favorites',
      {headers: this.headersService.get()}
    )
      .map(response => response)
      .catch((error: any) => Observable.throw(error));

  }

}
