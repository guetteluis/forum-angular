import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {HeadersService} from '../headers/headers.service';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {Response} from '../../interfaces/response';

@Injectable()
export class ChannelsService {

  protected apiUrl: string = environment.api_url;

  constructor(
    private http: HttpClient,
    private headersService: HeadersService,
  ) { }

  index(): Observable<Response> {

    return this.http.get(this.apiUrl + '/api/channels', {headers: this.headersService.set()})
      .map(response => response)
      .catch((error: any) => Observable.throw(error));

  }

}
