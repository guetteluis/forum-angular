import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {HeadersService} from '../headers/headers.service';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { Response } from '../../interfaces/response';
import * as CryptoJS from 'crypto-js';

@Injectable()
export class AuthService {

  apiUrl: string = environment.api_url;
  secretKey: string = environment.secret_key;

  constructor(
    private headersService: HeadersService,
    private http: HttpClient
  ) { }

  login(credentials: any): Observable<Response> {

    return this.http.post(this.apiUrl + '/oauth/token', credentials, {headers: this.headersService.set()})
      .map(response => response)
      .catch((error: any) => Observable.throw(error));

  }

  saveData(data): void {

    const encryptedData = CryptoJS.AES.encrypt(JSON.stringify(data), this.secretKey).toString();

    localStorage.setItem('token', encryptedData);

  }

  getLoggedInUser(): Observable<Response> {

    this.headersService.set();

    this.headersService.addAuthHeader(this.getTokenType() + ' ' + this.getToken());

    return this.http.get(this.apiUrl + '/api/users/current', {headers: this.headersService.get()})
      .map(response => response)
      .catch((error: any) => Observable.throw(error));

  }

  refreshToken(credentials: any): Observable<Response> {

    return this.http.post(this.apiUrl + '/oauth/token', credentials, {headers: this.headersService.set()})
      .map(response => response)
      .catch((error: any) => Observable.throw(error));

  }

  loggedIn(): boolean {

    return !!this.getToken();

  }

  logout(): void {

    localStorage.clear();

  }

  getToken(): string {

    const data = this.loadData();

    if (data) {
      return data.access_token;
    }

    return null;

  }

  getTokenType(): string {

    const data = this.loadData();

    if (data) {
      return data.token_type;
    }

    return null;

  }

  getRefreshToken(): string {

    const data = this.loadData();

    if (data) {
      return data.refresh_token;
    }

    return null;

  }

  tokenExpired(): boolean {

    const data = this.loadData();

    if (data) {

      const expiresIn = data.expires_in * 1000;
      const currentDate = new Date().getTime();
      const tokenDate = new Date(data.date).getTime();

      return currentDate - tokenDate > expiresIn;

    }

    return true;

  }

  private loadData(): any {

    const encryptedToken = localStorage.getItem('token');

    if (encryptedToken) {

      const bytes = CryptoJS.AES.decrypt(encryptedToken, this.secretKey);

      return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));

    }

    return null;

  }

}
