import { TestBed, inject } from '@angular/core/testing';

import { ThreadSubscriptionsService } from './thread-subscriptions.service';

describe('ThreadSubscriptionsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ThreadSubscriptionsService]
    });
  });

  it('should be created', inject([ThreadSubscriptionsService], (service: ThreadSubscriptionsService) => {
    expect(service).toBeTruthy();
  }));
});
