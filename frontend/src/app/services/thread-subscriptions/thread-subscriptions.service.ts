import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Response} from '../../interfaces/response';
import {AuthService} from '../auth/auth.service';
import {HeadersService} from '../headers/headers.service';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Injectable()
export class ThreadSubscriptionsService {

  protected apiUrl: string = environment.api_url;

  constructor(
    private http: HttpClient,
    private headersService: HeadersService,
    private auth: AuthService
  ) { }

  store(channel_slug: string, thread_id: number): Observable<Response> {

    this.headersService.set();
    this.headersService.addAuthHeader(this.auth.getTokenType() + ' ' + this.auth.getToken());

    return this.http.post(
      this.apiUrl + '/api/threads/' + channel_slug + '/' + thread_id + '/subscriptions',
      {},
      {headers: this.headersService.get()}
    )
      .map(response => response)
      .catch((error: any) => Observable.throw(error));

  }

  destroy(channel_slug: string, thread_id: number): Observable<Response> {

    this.headersService.set();
    this.headersService.addAuthHeader(this.auth.getTokenType() + ' ' + this.auth.getToken());

    return this.http.delete(
      this.apiUrl + '/api/threads/' + channel_slug + '/' + thread_id + '/subscriptions',
      {headers: this.headersService.get()}
    )
      .map(response => response)
      .catch((error: any) => Observable.throw(error));

  }

}
