import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';

@Injectable()
export class HeadersService {

  protected headers: any;

  constructor() { }

  set(): HttpHeaders {

    this.headers = {
      'Content-Type': 'application/json',
      'X-Requested-With': 'XMLHttpRequest'
    };

    return new HttpHeaders(this.headers);
  }

  addAuthHeader(authToken: string): HttpHeaders {

    this.headers['Authorization'] = authToken;

    return new HttpHeaders(this.headers);
  }

  get(): HttpHeaders {

    return new HttpHeaders(this.headers);

  }
}
