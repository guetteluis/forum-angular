import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {HeadersService} from '../headers/headers.service';
import {environment} from '../../../environments/environment';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Reply} from '../../models/reply';
import {Observable} from 'rxjs/Observable';
import {AuthService} from '../auth/auth.service';
import {Response} from '../../interfaces/response';

@Injectable()
export class RepliesService {

  protected apiUrl: string = environment.api_url;

  constructor(
    private http: HttpClient,
    private headersService: HeadersService,
    private auth: AuthService
  ) { }

  index(channelSlug: string, threadId: number, queryParams: any): Observable<Response> {

    this.headersService.set();
    this.headersService.addAuthHeader(this.auth.getTokenType() + ' ' + this.auth.getToken());

    return this.http.get(this.apiUrl + '/api/threads/' + channelSlug + '/' + threadId + '/replies',
      {
        headers: this.headersService.get(),
        params: queryParams
      })
      .map(response => response)
      .catch((error: any) => Observable.throw(error));

  }

  add(channel_slug: string, thread_id: number, reply: Reply): Observable<Response> {

    this.headersService.set();
    this.headersService.addAuthHeader(this.auth.getTokenType() + ' ' + this.auth.getToken());

    return this.http.post(
      this.apiUrl + '/api/threads/' + channel_slug + '/' + thread_id + '/replies',
      reply,
      {headers: this.headersService.get()}
      )
      .map(response => response)
      .catch((error: any) => Observable.throw(error));

  }

  update(reply_id: number, body: string): Observable<Response> {

    this.headersService.set();
    this.headersService.addAuthHeader(this.auth.getTokenType() + ' ' + this.auth.getToken());

    return this.http.patch(
      this.apiUrl + '/api/replies/' + reply_id, {body: body} , {headers: this.headersService.get()}
    )
      .map(response => response)
      .catch((error: any) => Observable.throw(error));

  }

  destroy(reply_id: number): Observable<Response> {

    this.headersService.set();
    this.headersService.addAuthHeader(this.auth.getTokenType() + ' ' + this.auth.getToken());

    return this.http.delete(
      this.apiUrl + '/api/replies/' + reply_id, {headers: this.headersService.get()}
    )
      .map(response => response)
      .catch((error: any) => Observable.throw(error));

  }

}
