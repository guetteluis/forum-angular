import {Injectable} from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class ProgressBarService {

  private showOptionsSource = new Subject<any>();
  private hideOptionsSource = new Subject<any>();

  show$ = this.showOptionsSource.asObservable();
  hide$ = this.hideOptionsSource.asObservable();

  constructor() { }

  show(options: {
    mode: 'determinate' | 'indeterminate' | 'buffer' | 'query',
    color?: 'primary' | 'accent' | 'warn',
    bufferValue?: number,
    value?: number,
  }) {
    this.showOptionsSource.next(options);
  }

  hide() {
    this.hideOptionsSource.next();
  }

}
