import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {HeadersService} from '../headers/headers.service';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {Response} from '../../interfaces/response';
import {AuthService} from '../auth/auth.service';

@Injectable()
export class ProfilesService {

  protected apiUrl: string = environment.api_url;

  constructor(
    private http: HttpClient,
    private headersService: HeadersService,
    private auth: AuthService
  ) { }

  show(username: string, page?: number): Observable<Response> {

    this.headersService.set();
    this.headersService.addAuthHeader(this.auth.getTokenType() + ' ' + this.auth.getToken());

    let queryParams;
    if (page) {
      queryParams = {
        page: page
      };
    }

    return this.http.get(

      this.apiUrl + '/api/profiles/' + username,
      {headers: this.headersService.get(), params: queryParams}
      )
      .map(response => response)
      .catch((error: any) => Observable.throw(error));

  }

}
