import { Injectable } from '@angular/core';
import {Subject} from 'rxjs/Subject';

@Injectable()
export class FlashService {

  protected flashOptionsSource = new Subject<any>();

  show$ = this.flashOptionsSource.asObservable();

  constructor() { }

  show(options: {type: string, message: string, timeout?: number}) {
    this.flashOptionsSource.next(options);
  }

}
