import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import {AuthService} from '../../services/auth/auth.service';
import {ProgressBarService} from '../../services/progress-bar/progress-bar.service';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(
    private authService: AuthService,
    private router: Router,
    private progressBar: ProgressBarService
  ) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {

    this.progressBar.show({mode: 'indeterminate'});

    return this.authService.getLoggedInUser().map((response) => {

      this.progressBar.hide();
      return true;

    }).catch((error) => {

      this.progressBar.hide();
      this.router.navigate(['/login']);
      return Observable.of(false);

    });
  }

}
