import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import {ThreadsIndexComponent} from './components/threads-index/threads-index.component';
import {ThreadsShowComponent} from './components/threads-show/threads-show.component';
import {LoginComponent} from './components/login/login.component';
import {ThreadsCreateComponent} from './components/threads-create/threads-create.component';
import {AuthGuard} from './guards/auth/auth.guard';
import {ProfilesShowComponent} from './components/profiles-show/profiles-show.component';

const appRoutes: Routes = [
  {path: '', component: HomeComponent},

  {path: 'login', component: LoginComponent},

  {path: 'profiles/:username', component: ProfilesShowComponent},

  {path: 'threads',
    children: [
      {path: '', component: ThreadsIndexComponent},
      {path: 'create', component: ThreadsCreateComponent, canActivate: [AuthGuard]},
      {path: ':channelSlug', component: ThreadsIndexComponent},
      {path: ':channelSlug/:id', component: ThreadsShowComponent}
    ]
  }
];

export const AppRouting = RouterModule.forRoot(appRoutes);
