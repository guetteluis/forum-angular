import { Component } from '@angular/core';
import {ProgressBarService} from './services/progress-bar/progress-bar.service';
import {FlashService} from './services/flash/flash.service';
import {AuthService} from './services/auth/auth.service';
import {HeadersService} from './services/headers/headers.service';

@Component({
  selector: 'app-root',
  providers: [
    ProgressBarService,
    FlashService,
    AuthService,
    HeadersService
  ],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

}
