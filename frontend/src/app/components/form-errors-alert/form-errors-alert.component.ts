import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-form-errors-alert',
  templateUrl: './form-errors-alert.component.html',
  styleUrls: ['./form-errors-alert.component.css']
})
export class FormErrorsAlertComponent implements OnInit {

  @Input() errors: Array<string>;

  constructor() { }

  ngOnInit() {
  }

}
