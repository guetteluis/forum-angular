import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThreadsShowComponent } from './threads-show.component';

describe('ThreadsShowComponent', () => {
  let component: ThreadsShowComponent;
  let fixture: ComponentFixture<ThreadsShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThreadsShowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThreadsShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
