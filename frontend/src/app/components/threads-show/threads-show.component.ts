import {Component, OnDestroy, OnInit} from '@angular/core';
import {ThreadsService} from '../../services/threads/threads.service';
import {Thread} from '../../models/thread';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs/Subscription';
import {ProgressBarService} from '../../services/progress-bar/progress-bar.service';
import {AuthService} from '../../services/auth/auth.service';
import {FlashService} from '../../services/flash/flash.service';

@Component({
  selector: 'app-threads-show',
  templateUrl: './threads-show.component.html',
  styleUrls: ['./threads-show.component.css']
})
export class ThreadsShowComponent implements OnInit, OnDestroy {

  thread: Thread;
  routeSubscription: Subscription;
  channelSlug: string;
  threadId: number;

  constructor(
    private threads: ThreadsService,
    private route: ActivatedRoute,
    private router: Router,
    private progressBar: ProgressBarService,
    private auth: AuthService,
    private flash: FlashService
  ) { }

  ngOnInit() {

    this.progressBar.show({mode: 'indeterminate'});
    this.getRouteParams();

  }

  get(page?: number): void {

    this.threads.show(this.channelSlug, this.threadId, {page: page}).subscribe((response) => {

      this.thread = response.thread;
      this.progressBar.hide();

    }, (error) => {

      this.flash.show({type: 'danger', message: 'Communication error. Please, try again'});
      this.progressBar.hide();

    });

  }

  delete(): void {

    this.threads.destroy(this.channelSlug, this.threadId).subscribe((response) => {

      this.router.navigate(['/threads']);
      this.flash.show({type: 'success', message: 'Thread has been deleted'});

    }, (error) => {
      this.flash.show({type: 'danger', message: 'Thread not deleted. Please, try again'});
    });

  }

  ngOnDestroy() {
    this.routeSubscription.unsubscribe();
  }

  protected getRouteParams(): void {

    this.routeSubscription = this.route.params.subscribe((params) => {

      this.channelSlug = params['channelSlug'];
      this.threadId = params['id'];

      this.get();

    });

  }

}
