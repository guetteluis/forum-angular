import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {environment} from '../../../environments/environment';
import {AuthService} from '../../services/auth/auth.service';
import {Router} from '@angular/router';
import {ProgressBarService} from '../../services/progress-bar/progress-bar.service';
import {FlashService} from '../../services/flash/flash.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  errors: Array<string> = [];

  constructor(
    private formBuilder: FormBuilder,
    private auth: AuthService,
    private router: Router,
    private progressBar: ProgressBarService,
    private flash: FlashService
  ) { }

  ngOnInit() {

    this.buildLoginForm();

  }

  protected buildLoginForm(): void {

    this.loginForm = this.formBuilder.group({

      'email': new FormControl('', [
        Validators.required,
        Validators.maxLength(30),
        Validators.pattern('^[a-z][a-zA-Z0-9_.]*(\\.[a-zA-Z][a-zA-Z0-9_.]*)?@[a-z0-9][a-zA-Z0-9]*\\.[a-z]+(\\.[a-z]+)?$')
      ]),
      'password': new FormControl('', [
        Validators.required,
      ])

    });

  }

  invalidInput(fieldName: string): boolean {

    return !this.loginForm.controls[fieldName].valid;

  }

  onLoginSubmit(): void {
    this.progressBar.show({mode: 'indeterminate'});
    const formControls = this.loginForm.controls;

    const credentials = {
      grant_type: 'password',
      client_id: environment.api_client_id,
      client_secret: environment.api_client_secret,
      username: formControls['email'].value,
      password: formControls['password'].value
    };


    this.auth.login(credentials).subscribe((response) => {

      this.auth.saveData(response);
      this.flash.show({type: 'info', message: 'Welcome back!'});
      this.progressBar.hide();
      this.router.navigate(['/']);

    }, (errors) => {

      this.errors = errors;
      this.progressBar.hide();

    });
  }

}
