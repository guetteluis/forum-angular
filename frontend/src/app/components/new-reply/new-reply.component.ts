import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Reply} from '../../models/reply';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ProgressBarService} from '../../services/progress-bar/progress-bar.service';
import {RepliesService} from '../../services/replies/replies.service';
import {Thread} from '../../models/thread';
import {FlashService} from '../../services/flash/flash.service';
import {AuthService} from '../../services/auth/auth.service';

declare let $: any;

@Component({
  selector: 'app-new-reply',
  templateUrl: './new-reply.component.html',
  styleUrls: ['./new-reply.component.css']
})
export class NewReplyComponent implements OnInit {

  @Input() thread: Thread;

  @Output() onStore = new EventEmitter<any>();

  storeForm: FormGroup;
  errors: Array<string>;

  constructor(
    private formBuilder: FormBuilder,
    private progressBar: ProgressBarService,
    private flash: FlashService,
    private replies: RepliesService,
    private auth: AuthService,
  ) { }

  ngOnInit() {
    this.buildAddReplyForm();
  }

  protected buildAddReplyForm(): void {

    this.storeForm = this.formBuilder.group({

      'body': new FormControl('', [
        Validators.required
      ])

    });

  }

  findChoices(searchText: string) {

    if (searchText.length >= 1) {

      return new Promise((resolve) => {

        $.get(`http://forum.test/api/users?name=${searchText}`, (response, status) => {
          resolve(response.users.filter(item =>
              item.toLowerCase().includes(searchText.toLowerCase())
            )
          );
        });

      });

    }

  }

  getChoiceLabel(choice: string) {
    return `@${choice} `;
  }

  protected invalidInput(fieldName: string): boolean {
    return !this.storeForm.controls[fieldName].valid && this.storeForm.controls[fieldName].touched;
  }

  protected isLoggedIn(): boolean {
    return this.auth.loggedIn();
  }

  protected onSubmit(): void {

    this.progressBar.show({mode: 'indeterminate'});
    const reply: Reply = {
      body: this.storeForm.controls['body'].value
    };

    this.replies.add(this.thread.channel.slug, this.thread.id, reply).subscribe((response) => {

      this.onStore.emit();
      this.flash.show({type: 'success', message: 'Reply has been posted'});
      this.progressBar.hide();
      this.storeForm.reset();
      this.storeForm.clearValidators();
      window.scrollTo(0, 0);

    }, (errors) => {

      this.progressBar.hide();
      this.errors = errors;

    });

  }

}
