import {Component, Input, OnInit} from '@angular/core';
import {User} from '../../models/user';

@Component({
  selector: 'app-activities',
  templateUrl: './activities.component.html',
  styleUrls: ['./activities.component.css']
})
export class ActivitiesComponent implements OnInit {

  @Input() type: string;
  @Input() profile: User;
  @Input() data: any;


  constructor() { }

  ngOnInit() {
  }

}
