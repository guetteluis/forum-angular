import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';
import {ProgressBarService} from '../../services/progress-bar/progress-bar.service';

@Component({
  selector: 'app-progress-bar',
  templateUrl: './progress-bar.component.html',
  styleUrls: ['./progress-bar.component.css']
})
export class ProgressBarComponent implements OnInit, OnDestroy {

  show = false;
  mode: 'determinate' | 'indeterminate' | 'buffer' | 'query' = 'indeterminate';
  color: 'primary' | 'accent' | 'warn' = 'primary';
  bufferValue: number;
  value: number;

  protected showSubscription: Subscription;
  protected hideSubscription: Subscription;

  constructor(
    private progressBar: ProgressBarService
  ) {

    this.showSubscription = this.progressBar.show$.subscribe((options) => {
        this.show = true;
        this.mode = options.mode;
        this.bufferValue = options.bufferValue;
        this.value = options.value;
    });

    this.hideSubscription = this.progressBar.hide$.subscribe(() => {

      this.show = false;

    });

  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.showSubscription.unsubscribe();
    this.hideSubscription.unsubscribe();
  }

}
