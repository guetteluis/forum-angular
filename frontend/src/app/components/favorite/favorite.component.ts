import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Reply} from '../../models/reply';
import {User} from '../../models/user';
import {AuthService} from '../../services/auth/auth.service';
import {FavoritesService} from '../../services/favorites/favorites.service';
import {FlashService} from '../../services/flash/flash.service';

@Component({
  selector: 'app-favorite',
  templateUrl: './favorite.component.html',
  styleUrls: ['./favorite.component.css']
})
export class FavoriteComponent implements OnInit {

  @Input() reply: Reply;

  @Output() onUpdate = new EventEmitter<any>();
  @Output() onDestroy = new EventEmitter<any>();

  constructor(
    private favorites: FavoritesService,
    private flash: FlashService,
    private auth: AuthService
  ) { }

  ngOnInit() {
  }

  isloggedIn(): boolean {
    return this.auth.loggedIn();
  }

  ifFavorited(favorited: boolean): any {
    return { 'text-secondary': favorited  };
  }

  favorite(reply: Reply): void {
    return reply.favorited ? this.destroy(reply) : this.create(reply);
  }

  protected destroy(reply: Reply): void {

    this.favorites.destroy(reply.id).subscribe((response) => {

      this.flash.show({type: 'success', message: 'You have unmarked a favorited reply'});
      this.onDestroy.emit();

    });

  }

  protected create(reply: Reply): void {

    this.favorites.store(reply.id).subscribe((response) => {

      this.flash.show({type: 'success', message: 'You have marked as favorite a reply'});
      this.onUpdate.emit();

    });

  }

}
