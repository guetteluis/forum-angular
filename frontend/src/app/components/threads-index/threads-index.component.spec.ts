import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThreadsIndexComponent } from './threads-index.component';

describe('ThreadsIndexComponent', () => {
  let component: ThreadsIndexComponent;
  let fixture: ComponentFixture<ThreadsIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThreadsIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThreadsIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
