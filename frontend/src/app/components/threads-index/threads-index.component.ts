import {Component, OnDestroy, OnInit} from '@angular/core';
import {ThreadsService} from '../../services/threads/threads.service';
import {Thread} from '../../models/thread';
import {ProgressBarService} from '../../services/progress-bar/progress-bar.service';
import {Subscription} from 'rxjs/Subscription';
import {ActivatedRoute} from '@angular/router';
import {FlashService} from '../../services/flash/flash.service';

@Component({
  selector: 'app-threads-index',
  templateUrl: './threads-index.component.html',
  styleUrls: ['./threads-index.component.css']
})
export class ThreadsIndexComponent implements OnInit, OnDestroy {

  threadsData: any;
  threads: Array<Thread>;
  routeParamsSubscription: Subscription;
  routeQueryParamsSubscription: Subscription;
  channelSlug: string;
  queryParams: {
    page?: string,
    by?: string,
    popular?: string,
    unanswered?: string
  };

  constructor(
    private threadsService: ThreadsService,
    private progressBar: ProgressBarService,
    private route: ActivatedRoute,
    private flash: FlashService
  ) {
    this.queryParams = {};
  }

  ngOnInit() {

    this.progressBar.show({ mode: 'indeterminate' });
    this.getRouteParams();

  }

  protected getPage(page: string): void {

    this.queryParams.page = page;

    this.getRouteParams();

  }

  protected getRouteParams(): void {

    this.routeParamsSubscription = this.route.params.subscribe((params) => {

      if (params['channelSlug']) {
        this.channelSlug = params['channelSlug'];
      }

      this.getRouteQueryParams();

    });

  }

  protected getRouteQueryParams(): void {

    this.routeQueryParamsSubscription = this.route.queryParams.subscribe((params) => {

      if (params['by']) {
        this.queryParams.by = params['by'];
      }

      if (params['popular']) {
        this.queryParams.popular = params['popular'];
      }

      if (params['unanswered']) {
        this.queryParams.unanswered = params['unanswered'];
      }

      this.threadsIndex(this.queryParams, this.channelSlug);

    });

  }

  protected threadsIndex(queryParams: any, channelSlug?: string): void {
    
    this.threadsService.index(queryParams, channelSlug).subscribe((response) => {

      this.threadsData = response.threads_data;
      this.threads = this.threadsData.items;
      this.progressBar.hide();

    }, (errors) => {

      this.flash.show({type: 'danger', message: 'Communication error. Please, try again'});
      this.progressBar.hide();

    });

  }

  ngOnDestroy() {

    this.routeParamsSubscription.unsubscribe();
    this.routeQueryParamsSubscription.unsubscribe();

  }

}
