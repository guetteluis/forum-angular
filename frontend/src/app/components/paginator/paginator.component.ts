import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.css']
})
export class PaginatorComponent implements OnInit {

  @Input() currentPage: number;
  @Input() lastPage: number;

  @Output() getPage = new EventEmitter<number>();

  pagination = [];

  constructor() { }

  ngOnInit() {
    for (let i = 1; i <= this.lastPage; i++){
      this.pagination.push(i);
    }
  }

  onRefresh(page: number): void {

    window.scrollTo(0, 0);
    this.getPage.emit(page);

  }

}
