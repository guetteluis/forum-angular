import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Thread} from '../../models/thread';
import {ThreadSubscriptionsService} from '../../services/thread-subscriptions/thread-subscriptions.service';
import {FlashService} from '../../services/flash/flash.service';

@Component({
  selector: 'app-subscribe-button',
  templateUrl: './subscribe-button.component.html',
  styleUrls: ['./subscribe-button.component.css']
})
export class SubscribeButtonComponent implements OnInit {

  @Input() thread: Thread;

  @Output() onUpdate = new EventEmitter<any>();

  constructor(
    private threadSubscriptions: ThreadSubscriptionsService,
    private flash: FlashService
  ) { }

  ngOnInit() {
  }

  subscribe(): void {

    this.threadSubscriptions.store(this.thread.channel.slug, this.thread.id).subscribe(() => {

      this.flash.show({
        type: 'success',
        message: 'You have subscribed to a thread'
      });

      this.onUpdate.emit();

    });

  }

  unsubscribe(): void {

    this.threadSubscriptions.destroy(this.thread.channel.slug, this.thread.id).subscribe(() => {

      this.flash.show({
        type: 'success',
        message: 'You have unsubscribed from a thread'
      });

      this.onUpdate.emit();

    });

  }

}
