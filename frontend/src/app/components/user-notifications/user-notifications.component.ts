import {Component, Input, OnInit} from '@angular/core';
import {User} from '../../models/user';
import {NotificationsService} from '../../services/notifications/notifications.service';
import {Notification} from '../../models/notification';

@Component({
  selector: 'app-user-notifications',
  templateUrl: './user-notifications.component.html',
  styleUrls: ['./user-notifications.component.css']
})
export class UserNotificationsComponent implements OnInit {

  @Input() user: User;

  notifications: Array<Notification>;

  constructor(
    private notificationsService: NotificationsService
  ) { }

  ngOnInit() {
    this.get();
  }

  get(): void {

    this.notificationsService.index(this.user.name).subscribe((response) => {
      this.notifications = response.notifications;
    });

  }

  markAsRead(notificationId: string): void {

    this.notificationsService.destroy(this.user.name, notificationId).subscribe(() => {

      this.get();

    });

  }

}
