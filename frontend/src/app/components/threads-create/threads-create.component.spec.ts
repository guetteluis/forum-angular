import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThreadsCreateComponent } from './threads-create.component';

describe('ThreadsCreateComponent', () => {
  let component: ThreadsCreateComponent;
  let fixture: ComponentFixture<ThreadsCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThreadsCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThreadsCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
