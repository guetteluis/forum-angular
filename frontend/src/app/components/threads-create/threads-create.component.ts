import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Thread} from '../../models/thread';
import {ThreadsService} from '../../services/threads/threads.service';
import {Router} from '@angular/router';
import {ProgressBarService} from '../../services/progress-bar/progress-bar.service';
import {ChannelsService} from '../../services/channels/channels.service';
import {Channel} from '../../models/channel';
import {FlashService} from '../../services/flash/flash.service';

@Component({
  selector: 'app-threads-create',
  templateUrl: './threads-create.component.html',
  styleUrls: ['./threads-create.component.css']
})
export class ThreadsCreateComponent implements OnInit {

  channels: Array<Channel>;
  createThreadForm: FormGroup;
  errors: Array<string> = [];

  constructor(
    private formBuilder: FormBuilder,
    private threadsService: ThreadsService,
    private router: Router,
    private progressBar: ProgressBarService,
    private channelsService: ChannelsService,
    private flash: FlashService
  ) { }

  ngOnInit() {
    this.channelsIndex();
  }

  protected channelsIndex(): void {

    this.channelsService.index().subscribe((response) => {

      this.channels = response.channels;
      this.buildCreateThreadForm();

    });

  }

  protected buildCreateThreadForm(): void {

    this.createThreadForm = this.formBuilder.group({

      'title': new FormControl('', [
        Validators.required,
        Validators.maxLength(255)
      ]),
      'body': new FormControl('', [
        Validators.required
      ]),
      'channel_id': new FormControl('', [
        Validators.required
      ])

    });

  }

  invalidInput(fieldName: string): boolean {

    return !this.createThreadForm.controls[fieldName].valid;

  }

  onNewThreadSubmit(): void {

    this.progressBar.show({ mode: 'indeterminate' });

    const formControls = this.createThreadForm.controls;

    const newThread: Thread = {

      title: formControls['title'].value,
      body: formControls['body'].value,
      channel_id: formControls['channel_id'].value

    };

    this.threadsService.store(newThread).subscribe((response) => {

      this.progressBar.hide();
      this.flash.show({type: 'success', message: 'Your Thread has been published'});
      this.router.navigate(['/threads', response.thread.channel.name, response.thread.id]);

    }, (errors) => {

      this.errors = errors;
      this.progressBar.hide();

    });

  }

}
