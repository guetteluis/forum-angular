import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';
import {FlashService} from '../../services/flash/flash.service';

@Component({
  selector: 'app-flash',
  templateUrl: './flash.component.html',
  styleUrls: ['./flash.component.css']
})
export class FlashComponent implements OnInit, OnDestroy {

  type: string;
  body: string;
  show = false;

  showSubscription: Subscription;

  constructor(
    private flashService: FlashService
  ) {

    this.flashService.show$.subscribe((options) => {
      this.flash(options);
    });

  }

  ngOnInit() {

  }

  protected flash(options): void {

    this.body = options.message;
    this.type = options.type;
    this.show = true;

    this.hide(options.timeout);

  }

  protected hide(customTimeout) {

    let timeout = 3000;

    if (customTimeout) {
      timeout = customTimeout;
    }

    setTimeout(() => { this.show = false; }, timeout);

  }

  alertCssClasses() {
    return {
      'show': this.show,
      'hide': !this.show
    };
  }


  ngOnDestroy() {
    this.showSubscription.unsubscribe();
  }
}
