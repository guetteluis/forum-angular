import {Component, OnDestroy, OnInit} from '@angular/core';
import {ProfilesService} from '../../services/profiles/profiles.service';
import {User} from '../../models/user';
import {Subscription} from 'rxjs/Subscription';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-profiles-show',
  templateUrl: './profiles-show.component.html',
  styleUrls: ['./profiles-show.component.css']
})
export class ProfilesShowComponent implements OnInit, OnDestroy {

  username: string;
  profile: User;
  routeSubscription: Subscription;

  constructor(
    private profilesService: ProfilesService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.getRouteParams();
  }

  protected getRouteParams(): void {

    this.routeSubscription = this.route.params.subscribe((params) => {

      this.username = params['username'];
      this.getProfile(params['username']);

    });

  }

  protected getProfile(username: string, page?: number): void {

    this.profilesService.show(username, page).subscribe((response) => {
      this.profile = response.profile;
    }, () => {
      this.router.navigate(['/']);
    });

  }

  ngOnDestroy() {
    this.routeSubscription.unsubscribe();
  }

}
