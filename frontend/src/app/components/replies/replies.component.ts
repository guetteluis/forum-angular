import {Component, Input, OnInit} from '@angular/core';
import {Thread} from '../../models/thread';
import {RepliesService} from '../../services/replies/replies.service';
import {ProgressBarService} from '../../services/progress-bar/progress-bar.service';
import {FlashService} from '../../services/flash/flash.service';

@Component({
  selector: 'app-replies',
  templateUrl: './replies.component.html',
  styleUrls: ['./replies.component.css']
})
export class RepliesComponent implements OnInit {

  @Input() thread: Thread;

  repliesData: any;

  constructor(
    private replies: RepliesService,
    private progressBar: ProgressBarService,
    private flash: FlashService
  ) { }

  ngOnInit() {

    this.progressBar.show({mode: 'indeterminate'});
    this.get();

  }

  protected get(page?: number): void {

    this.replies.index(this.thread.channel.slug, this.thread.id, {page: page}).subscribe((response) => {

      this.repliesData = response.replies_data;
      this.progressBar.hide();

    }, (error) => {

      this.flash.show({type: 'danger', message: 'Communication error. Please, try again'});
      this.progressBar.hide();

    });

  }



}
