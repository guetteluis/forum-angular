import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth/auth.service';
import {Router} from '@angular/router';
import {ChannelsService} from '../../services/channels/channels.service';
import {Channel} from '../../models/channel';
import {User} from '../../models/user';
import {Observable} from 'rxjs/Observable';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  channels: Array<Channel> = [];
  loggedInUser: User;

  methodCalled = false;

  constructor(
    private auth: AuthService,
    private router: Router,
    private channelsService: ChannelsService
  ) { }

  ngOnInit() {
    this.channelsIndex();
    this.getLoggedInUser();
  }

  protected channelsIndex(): void {

    this.channelsService.index().subscribe((response) => {
      this.channels = response.channels;
    });

  }

  protected getLoggedInUser(): void {

    this.auth.getLoggedInUser().subscribe((response) => {

      this.loggedInUser = response.user;
      this.methodCalled = false;

    });

  }

  loggedIn(): boolean {
    if (!this.loggedInUser && this.auth.loggedIn() && !this.methodCalled) {

      this.methodCalled = true;
      this.getLoggedInUser();

    }
    return this.auth.loggedIn();
  }

  logout(): void {
    this.auth.logout();
    this.loggedInUser = null;
    this.router.navigate(['']);
  }

}
