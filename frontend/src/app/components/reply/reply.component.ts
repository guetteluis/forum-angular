import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {RepliesService} from '../../services/replies/replies.service';
import {FlashService} from '../../services/flash/flash.service';
import {Reply} from '../../models/reply';
import {ProgressBarService} from '../../services/progress-bar/progress-bar.service';

declare let $: any;

@Component({
  selector: 'app-reply',
  templateUrl: './reply.component.html',
  styleUrls: ['./reply.component.css']
})
export class ReplyComponent implements OnInit {

  @Input() reply: Reply;

  @Output() onDelete = new EventEmitter<any>();
  @Output() onUpdate = new EventEmitter<any>();

  replyEditedId: number;
  body: string;
  errors: Array<string>;

  constructor(
    private repliesService: RepliesService,
    private flash: FlashService,
    private progressBar: ProgressBarService
  ) { }

  ngOnInit() {
  }

  edit(reply: Reply): void {
    this.replyEditedId = reply.id;
    this.body = reply.body;
  }

  clearEdit(): void {

    this.replyEditedId = null;
    this.body = null;
    this.errors = null;
  }

  onEditSubmit(): void {

    this.progressBar.show({mode: 'indeterminate'});

    this.repliesService.update(this.replyEditedId, this.body).subscribe((response) => {

      this.onUpdate.emit();
      this.flash.show({type: 'success', message: 'Reply has been updated'});
      this.progressBar.hide();
      this.clearEdit();

    }, (errors) => {

      this.progressBar.hide();
      this.errors = errors;

    });

  }

  delete(replyId: number): void {

    this.repliesService.destroy(replyId).subscribe((response) => {

      $(document.getElementById('reply-' + replyId)).fadeOut(300, () => {

        this.flash.show({type: 'success', message: 'You have deleted a reply'});
        this.onDelete.emit();

      });

    });

  }

}
