const env = require('./config/env');
const express = require('express');
const helmet = require('helmet');
const compression = require('compression');
const cors = require('cors');
const bodyParser = require('body-parser');
const appRootPath = require('app-root-path');
const rateLimit = require('./config/rate-limit');
const routes = require('./config/routes');

// .env file configuration
env.get();

// Express initialization
const app = express();

// Helmet initialization
app.use(helmet());

// compress all responses
app.use(compression());

// CORS initialization
app.use(cors());

// Body parser middleware
app.use(bodyParser.json());

// Serve static public folder
app.use(express.static(appRootPath + '/public'));

//Basic rate-limiting middleware
app.enable('trust proxy');
app.use(rateLimit.limiter());

// Routes
app.use('/', routes);

const server = app.listen(process.env.PORT || 8080, () => {
  const port = server.address().port;
  console.log('app running on port', port);
});