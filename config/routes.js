const express = require('express');
const router = express.Router();
const path = require('path');
const contactController = require('../controllers/contact-controller');

router.post('/api/contact', contactController.sendMessage);

router.get('/', (request, response) => {
  response.send('Invalid Endpoint');
});

router.get('*', (request, response) => {
  response.sendFile(path.join(process.cwd(), 'public/index.html'));
});

module.exports = router;
